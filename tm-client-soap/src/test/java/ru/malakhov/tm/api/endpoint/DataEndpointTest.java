package ru.malakhov.tm.api.endpoint;

import marker.IntegrationCategory;
import org.junit.experimental.categories.Category;

@Category(IntegrationCategory.class)
public class DataEndpointTest {

//    private final Bootstrap bootstrap = new Bootstrap();
//    private Session adminSession;
//    private Session userSession;
//
//    @Before
//    public void initAdminSession() {
//        adminSession = bootstrap.getSessionEndpoint().open("admin", "admin");
//        bootstrap.getUserEndpoint().create("newUser", "newUser");
//        userSession = bootstrap.getSessionEndpoint().open("newUser", "newUser");
//        bootstrap.getProjectEndpoint().createProject(userSession, "project", "test");
//        bootstrap.getTaskEndpoint().createTask(userSession, "task", "test");
//    }
//
//    @After
//    public void clearData() {
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getSessionEndpoint().close(userSession);
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getSessionEndpoint().close(adminSession);
//    }
//
//    @Test
//    public void base64Test() throws Exception_Exception {
//        bootstrap.getDataEndpoint().base64Save(adminSession);
//        final File file = new File(DataConstant.DATA_BASE64_PATH);
//        Assert.assertTrue(file.exists());
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getDataEndpoint().base64Load(adminSession);
//        Assert.assertNotNull(bootstrap.getUserEndpoint().profile(userSession));
//        bootstrap.getDataEndpoint().base64Clear(adminSession);
//        Assert.assertFalse(file.exists());
//    }
//
//    @Test
//    public void binaryTest() throws Exception_Exception {
//        bootstrap.getDataEndpoint().binarySave(adminSession);
//        final File file = new File(DataConstant.DATA_BINARY_PATH);
//        Assert.assertTrue(file.exists());
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getDataEndpoint().binaryLoad(adminSession);
//        Assert.assertNotNull(bootstrap.getUserEndpoint().profile(userSession));
//        bootstrap.getDataEndpoint().binaryClear(adminSession);
//        Assert.assertFalse(file.exists());
//    }
//
//    @Test
//    public void jsonFasterxmlTest() throws Exception_Exception {
//        bootstrap.getDataEndpoint().jsonSaveFasterxml(adminSession);
//        final File file = new File(DataConstant.DATA_FASTERXML_JSON_PATH);
//        Assert.assertTrue(file.exists());
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getDataEndpoint().jsonLoadFasterxml(adminSession);
//        Assert.assertNotNull(bootstrap.getUserEndpoint().profile(userSession));
//        bootstrap.getDataEndpoint().jsonClearFasterxml(adminSession);
//        Assert.assertFalse(file.exists());
//    }
//
//    @Test
//    public void xmlFasterxmlTest() throws Exception_Exception {
//        bootstrap.getDataEndpoint().xmlSaveFasterxml(adminSession);
//        final File file = new File(DataConstant.DATA_FASTERXML_XML_PATH);
//        Assert.assertTrue(file.exists());
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getDataEndpoint().xmlLoadFasterxml(adminSession);
//        Assert.assertNotNull(bootstrap.getUserEndpoint().profile(userSession));
//        bootstrap.getDataEndpoint().xmlClearFasterxml(adminSession);
//        Assert.assertFalse(file.exists());
//    }
//
//    @Test
//    public void jsonJaxbTest() throws Exception_Exception {
//        bootstrap.getDataEndpoint().jsonSaveJaxb(adminSession);
//        final File file = new File(DataConstant.DATA_JAXB_JSON_PATH);
//        Assert.assertTrue(file.exists());
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getDataEndpoint().jsonLoadJaxb(adminSession);
//        Assert.assertNotNull(bootstrap.getUserEndpoint().profile(userSession));
//        bootstrap.getDataEndpoint().jsonClearJaxb(adminSession);
//        Assert.assertFalse(file.exists());
//    }
//
//    @Test
//    public void xmlJaxbTest() throws Exception_Exception {
//        bootstrap.getDataEndpoint().xmlSaveJaxb(adminSession);
//        final File file = new File(DataConstant.DATA_JAXB_XML_PATH);
//        Assert.assertTrue(file.exists());
//        bootstrap.getTaskEndpoint().removeOneTaskByName(userSession, "task");
//        bootstrap.getProjectEndpoint().removeOneByName(userSession, "project");
//        bootstrap.getAdminEndpoint().removeByLogin(adminSession, "newUser");
//        bootstrap.getDataEndpoint().xmlLoadJaxb(adminSession);
//        Assert.assertNotNull(bootstrap.getUserEndpoint().profile(userSession));
//        bootstrap.getDataEndpoint().xmlClearJaxb(adminSession);
//        Assert.assertFalse(file.exists());
//    }

}
