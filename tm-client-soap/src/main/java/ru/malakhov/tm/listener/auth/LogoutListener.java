package ru.malakhov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.endpoint.soap.AuthSoapEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class LogoutListener extends AbstractListener {

    @Autowired
    private AuthSoapEndpoint authSoapEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "logout";
    }

    @Override
    public @NotNull String description() {
        return "Logout.";
    }

    @Override
    @EventListener(condition = "@logoutListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        authSoapEndpoint.logout();
        sessionService.setCookieHeaders(null);
        System.out.println("[OK]");
    }

}
