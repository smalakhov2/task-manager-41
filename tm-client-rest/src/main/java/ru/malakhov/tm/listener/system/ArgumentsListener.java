package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;


import java.util.List;

@Component
public class ArgumentsListener extends AbstractListener {

    @Autowired
    private List<AbstractListener> commands;

    @Override
    public @NotNull String arg() {
        return "-arg";
    }

    @Override
    public @NotNull String command() {
        return "arguments";
    }

    @Override
    public @NotNull String description() {
        return "Display arguments.";
    }

    @Override
    @EventListener(condition = "@argumentsListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        for (final @NotNull AbstractListener command: commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
        System.out.println("[OK]");
    }

}