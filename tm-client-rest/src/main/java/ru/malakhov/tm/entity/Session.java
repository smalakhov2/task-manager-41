package ru.malakhov.tm.entity;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@NoArgsConstructor
public class Session extends AbstractEntity {

    private @Nullable Long timestamp;

    private @Nullable User user;

    private @Nullable String signature;

    public @Nullable Long getTimestamp() {
        return timestamp;
    }

    public @Nullable User getUser() {
        return user;
    }

    public @Nullable String getSignature() {
        return signature;
    }

}