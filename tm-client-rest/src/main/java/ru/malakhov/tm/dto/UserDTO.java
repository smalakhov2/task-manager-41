package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumeration.Role;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractDTO {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @Nullable
    private Role role = Role.USER;

    private boolean locked = false;

    public UserDTO(final @NotNull String login, final @NotNull String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(final @Nullable User user) {
        if (user == null) return;
        setId(user.getId());
        login = user.getLogin();
        passwordHash = user.getPasswordHash();
        email = user.getEmail();
        firstName = user.getFirstName();
        middleName = user.getMiddleName();
        lastName = user.getLastName();
        role = user.getRole();
        locked = user.isLocked();
    }

    @Override
    public String toString() {
        return "LOGIN: " + login + '\n' +
                "E-MAIL: " + email + '\n' +
                "FIRST-NAME: " + firstName + '\n' +
                "MIDDLE-NAME: " + middleName + '\n' +
                "LAST-NAME: " + lastName + '\n' +
                "ROLE: " + role;
    }

}