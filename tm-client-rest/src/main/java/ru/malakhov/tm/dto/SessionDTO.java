package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDTO extends AbstractDTO implements Cloneable {

    public static final long serialVersionUID = 1L;

    @Override
    public SessionDTO clone(){
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e){
            return null;
        }
    }

    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    public SessionDTO(final @Nullable Session session) {
        if (session == null) return;
        setId(session.getId());
        timestamp = session.getTimestamp();
        userId = session.getUser().getId();
        signature = session.getSignature();
    }

}