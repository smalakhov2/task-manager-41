package ru.malakhov.tm.api.endpoint;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.Task;

import java.util.List;

@RequestMapping("/api/tasks")
public interface ITasksRestEndpoint {

    static ITasksRestEndpoint tasksClient(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ITasksRestEndpoint.class, baseUrl);
    }

    @GetMapping
    List<TaskDTO> getListDTO();

    @PostMapping
    List<Task> saveAll(@RequestBody List<Task> list);

    @DeleteMapping("/")
    void deleteAll(@RequestBody List<Task> list);

    @DeleteMapping("/all")
    void deleteAll();

}