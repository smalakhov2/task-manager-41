package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.ITasksRestEndpoint;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping
    public List<TaskDTO> getListDTO() {
        return taskService.findAllDTOByUserId(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        taskService.deleteAllByUserId(UserUtil.getUserId());
    }

}
