package ru.malakhov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    public List<TaskDTO> getTasksListDTO() {
        return taskService.findAllDTOByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public void deleteAllTasks() {
        taskService.deleteAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    public void createTask(@WebParam(name = "name") String name) {
        taskService.create(UserUtil.getUserId(), name);
    }

    @WebMethod
    public void updateTask(@WebParam(name = "task") TaskDTO task) {
        taskService.updateDTO(UserUtil.getUserId(), task);
    }

    @WebMethod
    public TaskDTO findOneTaskByIdDTO(@WebParam(name = "id") String id) {
        return taskService.findOneByIdDTO(UserUtil.getUserId(), id);
    }

    @WebMethod
    public boolean existsTaskById(@WebParam(name = "id") String id) {
        return taskService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @WebMethod
    public void deleteOneTaskById(@WebParam(name = "id") String id) {
        taskService.removeOneById(UserUtil.getUserId(), id);
    }

}