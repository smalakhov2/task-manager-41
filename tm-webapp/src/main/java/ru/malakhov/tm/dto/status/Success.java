package ru.malakhov.tm.dto.status;

public final class Success extends Result {

    public Success() {
        success = true;
        message = "";
    }

}
