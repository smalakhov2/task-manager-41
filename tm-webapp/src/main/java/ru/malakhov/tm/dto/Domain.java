package ru.malakhov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.entity.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "Domain")
public class Domain implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotNull
    private List<Project> projectList = new ArrayList<>();

    @NotNull
    private List<Task> taskList = new ArrayList<>();

    @NotNull
    private List<User> userList = new ArrayList<>();

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public @NotNull List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(@NotNull List<Project> projectList) {
        this.projectList = projectList;
    }

    public @NotNull List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(@NotNull List<Task> taskList) {
        this.taskList = taskList;
    }

    public @NotNull List<User> getUserList() {
        return userList;
    }

    public void setUserList(@NotNull List<User> userList) {
        this.userList = userList;
    }
}
