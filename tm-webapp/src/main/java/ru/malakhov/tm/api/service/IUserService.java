package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {

    void create(final @Nullable String login, final @Nullable String password);

    void updateDTO(final @Nullable UserDTO user);

    void deleteAll();

    @NotNull
    List<User> findAll();

    @NotNull
    User findById(@Nullable String id);

    @Nullable
    UserDTO findByLoginDTO(@Nullable String login);

    @NotNull
    UserDTO profile(@Nullable String id);

}
