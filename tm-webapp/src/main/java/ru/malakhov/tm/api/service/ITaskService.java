package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void updateDTO(@Nullable String userId, @Nullable TaskDTO taskDTO);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserId(@Nullable String userId);

    void deleteAll();

    void deleteAllByUserId(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAllDTOByUserId(@Nullable String userId);

    @NotNull
    TaskDTO findOneByIdDTO(@Nullable String userId, @Nullable String id);

    void removeOneById(@Nullable String userId, @Nullable String id);

    boolean existsByUserIdAndId(@Nullable String userId, @Nullable String id);

}