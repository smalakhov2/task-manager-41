package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.TaskDTO;

import java.util.List;

@RequestMapping("/api/tasks")
public interface ITasksRestEndpoint {

    @GetMapping
    List<TaskDTO> getListDTO();

    @DeleteMapping("/all")
    void deleteAll();

}