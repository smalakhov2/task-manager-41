package ru.malakhov.tm.exception.task;

import ru.malakhov.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
