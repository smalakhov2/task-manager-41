package ru.malakhov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.dto.CustomUser;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.exception.user.UserNotFoundException;

@Service
public class UserDetailServiceBean implements UserDetailsService {

    @Autowired
    IUserService userService;

    @Override
    public UserDetails loadUserByUsername(final @Nullable String userName) {
        final @Nullable UserDTO user = userService.findByLoginDTO(userName);
        if(user == null) throw new UserNotFoundException();
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(new String[]{user.getRole().getDisplayName()})
                .build()).withUserId(user.getId());
    }

}