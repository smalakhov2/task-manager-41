package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Service
public final class PropertyService implements IPropertyService {

    private final @NotNull String NAME = "/application.properties";

    private final @NotNull Properties properties = new Properties();

    {
        try (final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public @NotNull String getServerHost() {
        final @NotNull String propertyHost = properties.getProperty("server.host");
        final @NotNull String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public @NotNull Integer getServerPort() {
        final @NotNull String propertyPort = properties.getProperty("server.port");
        final @NotNull String envPort = System.getProperty("server.port");
        @NotNull String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @Override
    public @NotNull Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @Override
    public @NotNull String getDatabaseDriver() {
        return properties.getProperty("datasource.driver");
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return properties.getProperty("datasource.url");
    }

    @Override
    public @NotNull String getDatabaseUsername() {
        return properties.getProperty("datasource.username");
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return properties.getProperty("datasource.password");
    }

    @Override
    public @NotNull String getDatabaseDialect() {
        return properties.getProperty("datasource.dialect");
    }
}