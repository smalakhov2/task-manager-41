package ru.malakhov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;
import ru.malakhov.tm.exception.empty.EmptyRoleException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.exception.user.AccessDeniedException;

@Service
public class AuthService implements IAuthService {

    @Autowired
    private IUserService userService;

    @Override
    public boolean checkRoles(final @Nullable String userId, final @Nullable Role[] roles) {
        if (roles == null || roles.length == 0) throw new EmptyRoleException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        final @Nullable User user = userService.findById(userId);
        final @Nullable Role role = user.getRole();
        for (final @Nullable Role item : roles) if (role.equals(item)) return true;
        throw new AccessDeniedException();
    }

    @Override
    public void registration(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        userService.create(login, password);
    }

}