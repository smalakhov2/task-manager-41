package ru.malakhov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.malakhov.tm.dto.CustomUser;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.exception.user.UserNotFoundException;

public class UserUtil {

    public static String getUserId() {
        final @NotNull Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final @NotNull Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException();
        if(!(principal instanceof CustomUser)) throw new UserNotFoundException();
        final @NotNull CustomUser customUser = (CustomUser)principal;
        return customUser.getUserId();
    }

}